import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { BrowserRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Provider } from 'react-redux'
import { StyleProvider } from '@ant-design/cssinjs'
import { store } from './store/index.ts'
// import { store } from './store/index.ts'

ReactDOM.createRoot(document.getElementById('root')!).render(
    <BrowserRouter>
        <Provider store={store}>
            <ToastContainer />
            <StyleProvider hashPriority="high">
                <App />
            </StyleProvider>
        </Provider>
    </BrowserRouter>
)
