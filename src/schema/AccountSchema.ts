import { z } from "zod";

export const AccountSchema = z.object({
    name: z.string().nonempty("Vui lòng nhập tên"),
    email: z
      .string()
      .nonempty("Vui lòng nhập địa chỉ email")
      .email("Vui lòng nhập đúng địa chỉ email"),
    password: z.string().nonempty("Vui lòng nhập mật khẩu"),
    phone: z.string().nonempty("Vui lòng nhập số điện thoại"),
    birthday: z.string().nonempty("Vui lòng chọn ngày sinh"),
});

export type AccountSchemaType = z.infer<typeof AccountSchema>;
