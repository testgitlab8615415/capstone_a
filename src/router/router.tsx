import { RouteObject } from 'react-router-dom'
// import { PATH } from ' constant/config'
import { Comment, Login, Register } from 'pages'
import { PATH } from 'constant'
import RoomByPosition from 'components/ui/RoomByPosition'
import PositionByID from 'components/ui/PositionByID'
import RoomByID from 'components/ui/RoomByID'
import Account from 'components/template/Account'
import AdminTemplate from 'components/template/AdminTemplate'
import { AuthLayout, MainLayout } from 'layouts'
export const router: RouteObject[] = [
  {
    path: "/",
    element: <MainLayout />
  },
  {
    path: PATH.comment,
    element: <Comment />

  },
  {
    path: PATH.roomByPosition,
    element: <RoomByPosition />

  },
  {
    path: PATH.positionByID,
    element: <PositionByID />

  },
  {
    path: PATH.roomByID,
    element: <RoomByID />

  },
  {
    path: PATH.account,
    element: <Account />,
  },
  {
    path: PATH.admin,
    element: <AdminTemplate />,
  },
  {
    element: <AuthLayout />,
    children: [
      {
        path: PATH.login,
        element: <Login />,
      },
      {
        path: PATH.register,
        element: <Register />,
      },

    ],
  },
]