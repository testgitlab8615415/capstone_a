import { apiInstance } from "constant";
import { Room, RoomDetail } from "types/Room";
// import { Room } from " types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_CYBERSOFT_API,
});
export const manageRoomService = {
  getRoom: () => api.get<ApiResponse<Room[]>>(`phong-thue`),
  getRoomByID: (query: any) => api.get<ApiResponse<RoomDetail>>(`phong-thue/${query}`),
  getRoomByPosition: (query: any) => api.get<ApiResponse<Room[]>>(`phong-thue/lay-phong-theo-vi-tri?maViTri=${query}`),
};
