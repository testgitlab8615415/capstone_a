import { apiInstance } from "constant";
import {  UserInfo } from "types";
import { DeletUser } from "types/User";

const api = apiInstance({
  baseURL: import.meta.env.VITE_CYBERSOFT_API,
});
export const manageUserService = {
  UserInfo:(query:any)=>api.get<ApiResponse<UserInfo>>(`users/${query}`),
  getUser:()=>api.get<ApiResponse<UserInfo[]>>(`/users`),
  deleteUser:(id:number)=>api.delete<ApiResponse<DeletUser[]>>(`/users?id=${id}`),

};
