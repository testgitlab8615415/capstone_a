import { apiInstance } from "constant";
import { Comment } from "types/Comment";

const api = apiInstance({
  baseURL: import.meta.env.VITE_BINH_LUAN_API,
});
export const manageCommentService = {
  getComments: () => api.get<ApiResponse<Comment[]>>("/binh-luan"),
};
