import { apiInstance } from "constant";
import { Position } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_CYBERSOFT_API,
});
export const managePositionService = {
  getPosition: () => api.get<ApiResponse<Position[]>>("/vi-tri"),
  getPositionByID: (query: any) => api.get<ApiResponse<Position>>(`/vi-tri/${query}`),

};
