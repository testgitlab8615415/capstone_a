import { apiInstance } from "constant";
import { LoginSchemaType, RegisterSchemaType } from "schema";
import { User } from "types";


const api = apiInstance({
  baseURL: "https://airbnbnew.cybersoft.edu.vn/api/auth",
});

export const authServices = {
  Register: (payload: RegisterSchemaType) => api.post("/signup", payload),
  Login: (payload: LoginSchemaType) =>api.post<ApiResponse<User>>("/signin", payload),
  
 
};
