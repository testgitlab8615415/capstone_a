import { apiInstance } from "constant";
import { Comment, CommentByRoomID, UserToken } from "types/Comment";

const api = apiInstance({
  baseURL: import.meta.env.VITE_CYBERSOFT_API,
});
export const manageCommentService = {
  getComments: () => api.get<ApiResponse<Comment[]>>("/binh-luan"),
  deleteComments: (id: number) => api.delete<ApiResponse<UserToken[]>>(`/binh-luan/${id}`),
  getCommentsByRoom: (query: any) => api.get<ApiResponse<CommentByRoomID[]>>(`/binh-luan/lay-binh-luan-theo-phong/${query}`),

};
