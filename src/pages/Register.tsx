import RegisterTemplates from "components/template/RegisterTemplates";

export const Register = () => {
  return (
    <div>
      <RegisterTemplates />
    </div>
  );
};

export default Register;
