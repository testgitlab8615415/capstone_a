export const PATH={
    admin:"/admin",
    login: "/login",
    account: "/account/:id",
    register: "/register",
    comment: '/comment',
    roomByPosition:'phong-thue/lay-phong-theo-vi-tri/:mavitri',
    positionByID:'/positionByID/:id',
    roomByID:'/phong-thue/:id'
}