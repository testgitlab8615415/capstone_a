// import { Footer, Header } from 'components/ui'
import { HomeRoom } from 'components/template'
import { Header } from 'components/ui'
import Footer from 'components/ui/Footer'
import { Outlet } from 'react-router-dom'
import { styled } from 'styled-components'

export const MainLayout = () => {
    return (
        <div>
            <Header />
            <Container>
                <HomeRoom />
                <Outlet />
                <Footer />
            </Container>

        </div>
    )
}

export default MainLayout

const Container = styled.main`
    // max-width: var(--max-width);
    margin: auto;
    margin-top: 60px;
`
