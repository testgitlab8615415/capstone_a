import {combineReducers}from '@reduxjs/toolkit'
import { RoomReducer } from './Room'
import { authReducer } from './auth'
import { PositionReducer } from './Position'
import { UserReducer } from './User'
import { CommentsReducer } from './Comment/slice'
export const rootReducer =combineReducers({
    manageComments:CommentsReducer,
    manageUser:UserReducer,
    managePosition:PositionReducer,
    manageRoom:RoomReducer,
    auth: authReducer,
})