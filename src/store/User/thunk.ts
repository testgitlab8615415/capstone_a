import { createAsyncThunk } from "@reduxjs/toolkit";
import { manageUserService } from "services/User";


export const getUserInfoThunk = createAsyncThunk(
  "getUserInfo/getUserInfoThunk",
      async (payload:any,{ rejectWithValue }) => {
        try {
        const query= payload
          const data = await manageUserService.UserInfo(query);
          return data.data.content;
        } 
        catch (error) {
          rejectWithValue(error);
        }
      }
    );
export const getUsersThunk = createAsyncThunk(
  "getUsers/getUsersThunk",
      async (_,{ rejectWithValue }) => {
        try {
          const data = await manageUserService.getUser();
          console.log(data.data.content)
          return data.data.content;
        } 
        catch (error) {
          rejectWithValue(error);
        }
      }
    );
    export const deletUsersThunk = createAsyncThunk(
      "user/deletUsersThunk",
      async (payload:number, { rejectWithValue }) => {
        try {
          const id=payload
          const data = await manageUserService.deleteUser(id);
          return data.data.content;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );