import { createSlice } from "@reduxjs/toolkit";
import {  UserInfo } from "types";
import { deletUsersThunk, getUserInfoThunk, getUsersThunk } from ".";
import { DeletUser } from "types/User";
type UserInitialState={
  userInfo?: UserInfo, 
  users:UserInfo[]| undefined,
  deleteUSers?: DeletUser[]
}
const initialState:UserInitialState={
  userInfo:{
   id: 0,
   name:"",
   email:"",
   password:"",
   phone:"",
   birthday:"",
   avatar:"",
   gender:false,
   role:""},
users:[],
deleteUSers:[]
}

export const UserSlice=createSlice({
    name:"users",
    initialState,
    reducers:{},
    extraReducers: (builder)=>{
        builder
        .addCase(getUserInfoThunk.fulfilled, (state, { payload }) => {
            state.userInfo = payload;
          })
        .addCase(getUsersThunk.fulfilled, (state, { payload }) => {
            state.users = payload;
          })
          .addCase(deletUsersThunk.fulfilled,(state,{payload})=>{
            state.deleteUSers=payload
            
          })


       
    }

});
export const {
    reducer:UserReducer,
    actions:UserAction
}=UserSlice