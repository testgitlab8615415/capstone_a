import { createSlice } from "@reduxjs/toolkit";

import { Room, RoomDetail,  } from "types/Room";
import { getRoomByIDThunk, getRoomByPositionThunk, getRoomThunk } from ".";
type RoomInitialState={
    roomByPosition:Room[]|undefined,
    roomByID:Room[]|undefined
    room:Room[]|undefined
    detailList?: RoomDetail;
    

}
const initialState:RoomInitialState={

    roomByPosition:[],
    roomByID:[],
    room:[],
    detailList:{
        id: 0,
        tenPhong: "",
        khach: 0,
        phongNgu: 0,
        giuong: 0,
        phongTam: 0,
        moTa: "",
        giaTien: 0,
        mayGiat: false,
        banLa: false,
        tivi: false,
        dieuHoa: false,
        wifi: false,
        bep: false,
        doXe: false,
        hoBoi: false,
        banUi: false,
        maViTri: 0,
        hinhAnh: ""
  
    }
}
export const RoomSlice=createSlice({
    name:"Room",
    initialState,
    reducers:{},
    extraReducers: (builder)=>{
        builder
        .addCase(getRoomByPositionThunk.fulfilled,(state,{payload})=>{
        state.roomByPosition=payload
        })
        .addCase(getRoomThunk.fulfilled,(state,{payload})=>{
            state.room=payload
        }
        )
        .addCase(getRoomByIDThunk.fulfilled,(state,{payload})=>{
    state.detailList=payload

        }
        )}

});
export const {
    reducer:RoomReducer,
    actions:RoomAction
}=RoomSlice