import { createAsyncThunk } from "@reduxjs/toolkit";
import { manageRoomService } from "services/Room";

export const getRoomByPositionThunk = createAsyncThunk(
"getRoom/getRoomByPositionThunk",
    async (payload:any,{ rejectWithValue }) => {
      try {
      const query= payload
        const data = await manageRoomService.getRoomByPosition(query);
        return data.data.content;
      } 
      catch (error) {
        rejectWithValue(error);
      }
    }
  );

export const getRoomByIDThunk = createAsyncThunk(
"getRoomID/getRoomByIDThunk",
    async (payload:any,{ rejectWithValue }) => {
      try {
      const query= payload
        const data = await manageRoomService.getRoomByID(query);
        return data.data.content;

      } 
      catch (error) {
        rejectWithValue(error);
      }
    }
  );


export const getRoomThunk = createAsyncThunk(
"room/getRoomThunk",
    async (_,{ rejectWithValue }) => {
      try {
      
        const data = await manageRoomService.getRoom();
        return data.data.content;
      } 
      catch (error) {
        rejectWithValue(error);
      }
    }
  );
 