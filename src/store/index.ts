import { configureStore } from '@reduxjs/toolkit'
// import { rootReducer } from './rootReducer'
import { useDispatch } from 'react-redux'
import { rootReducer } from './rootReducer'
// import {  getUserThunk } from './quanLyNguoiDung/thunk'

export const store = configureStore({
    reducer: rootReducer,
})
//được dispatch mỗi khi vào trang 

export type RootState = ReturnType<(typeof store)['getState']>

type AppDispatch = (typeof store)['dispatch']

export const useAppDispatch: () => AppDispatch = useDispatch
