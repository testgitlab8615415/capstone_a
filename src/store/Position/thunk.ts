import { createAsyncThunk } from "@reduxjs/toolkit";
import { managePositionService } from "services/Position";

export const getPositionThunk = createAsyncThunk(
"position/PositionThunk",
    async (_,{ rejectWithValue }) => {
      try {
      
        const data = await managePositionService.getPosition();
        return data.data.content;
      } 
      catch (error) {
        rejectWithValue(error);
      }
    }
  );
 
  export const getPositionByIDThunk = createAsyncThunk(
    "getPosition/getPositionByIDThunk",
        async (payload:any,{ rejectWithValue }) => {
          try {
          const query= payload
            const data = await managePositionService.getPositionByID(query);
            return data.data.content;
          } 
          catch (error) {
            rejectWithValue(error);
          }
        }
      );
 