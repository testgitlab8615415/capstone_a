import { createSlice } from "@reduxjs/toolkit";
import { Position } from "types";
import { getPositionByIDThunk, getPositionThunk } from ".";
type PositionInitialState={
    position:Position[]|undefined,
    positionByID?: Position,
    isFetchingPosition:boolean,
    

}
const initialState:PositionInitialState={
    position:[],
    positionByID:{
      
            id:0,
            tenViTri:"",
            tinhThanh:"",
            quocGia:"",
            hinhAnh:"",
        
    },
    isFetchingPosition:false
}
export const PositionSlice=createSlice({
    name:"Position",
    initialState,
    reducers:{},
    extraReducers: (builder)=>{
        builder
        .addCase(getPositionByIDThunk.fulfilled,(state,{payload})=>{
        state.positionByID=payload
        })
        
        .addCase(getPositionThunk.pending,(state)=>{
        state.isFetchingPosition=true
        })
        .addCase(getPositionThunk.fulfilled,(state,{payload})=>{
        state.position=payload
        state.isFetchingPosition=false
        })
        .addCase(getPositionThunk.rejected,(state)=>{
        state.isFetchingPosition=false
        })
     }

 });
export const {
    reducer:PositionReducer,
    actions:PositionAction
}=PositionSlice