import { createAsyncThunk } from "@reduxjs/toolkit";
import { LoginSchemaType } from "schema";
import { authServices } from "services";


export const loginThunk = createAsyncThunk(
  "auth/authThunk",
  async (payload: LoginSchemaType, { rejectWithValue }) => {
    try {
      const data = await authServices.Login(payload);
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

