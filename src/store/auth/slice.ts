import { createSlice } from "@reduxjs/toolkit";
import {   loginThunk } from "./thunk";
import { User } from "types";

type AuthInitialState = {
  user?: User,
  currentPage:number
  
};

const storedUser = localStorage.getItem("accessToken");
const initialState: AuthInitialState = {
  user: storedUser ? JSON.parse(storedUser) : undefined,
  currentPage:1
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
    logOut: (state) => {
      state.user = undefined;
      localStorage.removeItem("accessToken");
    },
  },
  extraReducers: (builder) => {
    builder
    .addCase(loginThunk.fulfilled, (state, { payload }) => {
      state.user = payload;
      if (payload) {
        localStorage.setItem("token", payload.token);
        localStorage.setItem("accessToken", JSON.stringify(payload));
      }
    })

  },
});

export const { reducer: authReducer, actions: authAction } = authSlice;
