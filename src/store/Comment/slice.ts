import { createSlice } from "@reduxjs/toolkit";
import { deleteCommentsThunk, getCommentsByRoomThunk, getCommentsThunk } from "./thunk";
import { Comment, CommentByRoomID, UserToken } from "types/Comment";
type CommentInitialState={
    comment:Comment[]|undefined,
    commentByRoom:CommentByRoomID[]|undefined,
    deleteComment: UserToken[]|undefined 
}
const initialState:CommentInitialState={
comment:[],
commentByRoom:[],
deleteComment:[]
}
export const CommentSlice=createSlice({
    name:"comments",
    initialState,
    reducers:{},
    extraReducers: (builder)=>{
        builder
        .addCase(getCommentsThunk.fulfilled,(state,{payload})=>{
        state.comment=payload  
        })
        .addCase(getCommentsByRoomThunk.fulfilled,(state,{payload})=>{
        state.commentByRoom=payload  
        })
        .addCase(deleteCommentsThunk.fulfilled,(state,{payload})=>{
        state.deleteComment=payload  
        })
    }

});
export const {
    reducer:CommentsReducer,
    actions:CommentsAction
}=CommentSlice