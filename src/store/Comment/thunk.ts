import { createAsyncThunk } from "@reduxjs/toolkit";
import { manageCommentService } from "services/Comment";

export const getCommentsThunk = createAsyncThunk(
"binhluan/commentsThunk",
    async (_,{ rejectWithValue }) => {
      try {
      
        const data = await manageCommentService.getComments();
        return data.data.content;
      } 
      catch (error) {
        rejectWithValue(error);
      }
    }
  );
 
  export const getCommentsByRoomThunk = createAsyncThunk(
    "getCommentsByRoom/getCommentsByRoomThunk",
        async (payload:any,{ rejectWithValue }) => {
          try {
          const query= payload
            const data = await manageCommentService.getCommentsByRoom(query);
            return data.data.content;
          } 
          catch (error) {
            rejectWithValue(error);
          }
        }
      );
      export const deleteCommentsThunk = createAsyncThunk(
        "comment/deleteCommentsThunk",
        async (payload:number, { rejectWithValue }) => {
          try {
            const id=payload
            const data = await manageCommentService.deleteComments(id);
            return data.data.content;
          } catch (err) {
            return rejectWithValue(err);
          }
        }
      );