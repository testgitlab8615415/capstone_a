import { useSelector } from "react-redux"
import { RootState } from " store"

export const useAuth=()=>{
  const { userInfo,users,deleteUSers } = useSelector((state: RootState) => state.manageUser);
  const { user} = useSelector((state: RootState) => state.auth);
    const {comment,commentByRoom} =useSelector((state:RootState)=>state.manageComments) 
    const {roomByPosition,room,detailList,roomByID} =useSelector((state:RootState)=>state.manageRoom) 
    const {position,isFetchingPosition,positionByID}=useSelector((state:RootState)=>state.managePosition)
    
    return{deleteUSers,user,comment,position,isFetchingPosition,roomByPosition,room,positionByID,detailList,roomByID,userInfo,commentByRoom,users}
}