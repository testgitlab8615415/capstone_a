export type User = {
  user: {
    id: number;
    name: string;
    email: string;
    password: string;
    phone: string;
    birthday: string;
    avatar: string,
    gender: boolean;
    role: string;  
    };
    token: string;
};
export type UserInfo = {

    id: number;
    name: string;
    email: string;
    password: string;
    phone: string;
    birthday: string;
    avatar: string,
    gender: boolean;
    role: string;  

};


