export type Comment={
    id:number,
    maPhong:number,
    maNguoiBinhLuan:number,
    ngayBinhLuan:string,
    noiDung:string,
    saoBinhLuan:number
}
export type CommentByRoomID={
    id: number,
    ngayBinhLuan:string,
    noiDung: string,
    saoBinhLuan: number,
    tenNguoiBinhLuan:string,
    avatar: string
}
export type UserToken = {
message:string
};