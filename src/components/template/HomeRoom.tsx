import { useAuth } from "hooks/useAuth"
import { useAppDispatch } from "store"
import { useEffect } from "react";
import { getPositionThunk } from "store/Position";
import { generatePath, useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { getRoomThunk } from "store/Room";
import { Card } from "components/ui";
import Skeleton from "components/ui/Skeleton";


export const HomeRoom = () => {
  var path = " "
  const { position, isFetchingPosition, room } = useAuth()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  useEffect(() => {
    dispatch((getPositionThunk()))
    dispatch(getRoomThunk())
  }, [dispatch])
  if (isFetchingPosition) {
    return (
      <div className="grid grid-cols-4 gap-50 " style={{ display: "grid", gridColumn: 4, gap: 50 }}>
        {[...Array(16)].map((_, index) => {
          return (
            <Card key={index} className="!w-[280px]">
              <Skeleton.Image active className="!w-full !h-[250px]" />
              <Skeleton.Input active className="!w-full !mt-10" />
              <Skeleton.Input active className="!w-full !mt-10" />
            </Card>
          );
        })}
      </div>
    );
  }



  return (
    <div>

      <h1>Địa Điểm </h1>
      <div style={{ maxWidth: 1000, display: "flex", margin: "auto", flexWrap: "wrap" }}>{

        position?.map((position) => {
          return (
            <div className="Container" key={position.id} style={{ width: 200 }} >
              <div className="container_uni " style={{ display: "" }}>
                <div className="container-item"  >
                  <img key={position.id} onClick={() => {
                    path = generatePath(PATH.roomByPosition, {
                      mavitri: position.id,
                    });
                    navigate(path);
                  }} style={{ width: 150, height: 150, borderRadius: 15 }} src={position.hinhAnh} alt="..." />
                </div>
                <div>
                  <h3>Quốc Gia: {position.quocGia}</h3>
                  <h3>Tỉnh/Thành Phố: <span style={{ color: "red" }}>{position.id}</span> </h3>
                  <h3>Quận/Huyện : <span style={{ color: "red" }}>{position.tenViTri}</span> </h3>
                </div>
              </div>
            </div>
          )
        })
      }</div>

      <div>
        {
          room?.map((room) => {
            return (
              <div key={room.id} onClick={() => {
                path = generatePath(PATH.roomByPosition, {
                  mavitri: room.maViTri,
                });
                navigate(path);
              }} >
                <div className="" >
                  <img className="w-96 " src={room.hinhAnh} alt="" />
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}

export default HomeRoom
