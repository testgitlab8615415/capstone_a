import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { LoginSchema, LoginSchemaType } from "schema";
import { useAppDispatch } from "store";
import { loginThunk } from "store/auth";
import { PATH } from "constant";
import { Input } from "components/ui";

const LoginTemplates = () => {
  const navigate = useNavigate();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });

  const dispatch = useAppDispatch();

  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    dispatch(loginThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng nhập thành công");
        navigate("/");
      });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-white text-40 font-600">Đăng nhập</h2>
      <div className="mt-30">
        <Input
          register={register}
          name="email"
          error={errors?.email?.message}
          placeholder="Email"
          type="email"
        />

        <div className="mt-20">
          <Input
            register={register}
            name="password"
            error={errors?.password?.message}
            placeholder="Mật khẩu"
            type="password"
          />
        </div>
        <div className="mt-16">
          <button className="text-white font-700  bg-red-500 w-full hover:bg-red-800 rounded-lg text-sm px-5 py-[15px]">
            Đăng nhập
          </button>
          <div></div>
          <p className="mt-10 text-white">
            Bạn mới tham gia?{" "}
            <span
              className="text-blue-500 cursor-pointer"
              onClick={() => {
                navigate(PATH.register);
              }}
            >
              Đăng ký ngay
            </span>
          </p>
        </div>
      </div>
    </form>
  );
};

export default LoginTemplates;
