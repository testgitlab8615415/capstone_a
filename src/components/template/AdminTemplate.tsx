import { useEffect } from "react"
import { useAuth } from "hooks"
import { useAppDispatch } from "store"
import { deletUsersThunk, getUsersThunk } from "store/User"
import { toast } from "react-toastify"
import { authAction } from "store/auth"
import Pagination from "components/ui/Pagination"

const AdminTemplate = () => {
  const dispatch = useAppDispatch()
  const { users, } = useAuth()
  const handlePageChange = (page: number) => {
    dispatch(authAction.setCurrentPage(page));
  };
  useEffect(() => {
    dispatch(getUsersThunk())
  }, [dispatch],)
  const handleDeleteUser = (userId: number, role: string) => {
    if (role == "USER" || role == "User") {
      dispatch(deletUsersThunk(userId))
      dispatch(getUsersThunk())
      toast.success(`Deleted user successfully`)
    } else {
      toast.error(`Role Admin can not be deleted`)
    }


  }

  return (
    <div>

      {

        users?.map((user) => {
          return (
            <div className="flex mx-3" key={user.id}>

              <h3 className=" mx-3"> {user.email}</h3>
              <h3 className=" mx-3">{user.name}</h3>
              <h3 className=" mx-3">{user.role}</h3>
              <button onClick={() => {

                handleDeleteUser(user.id, user.role)

              }} className="bg-red-500 ">DELETE</button>

            </div>
          )
        })

      }
      <Pagination
        defaultCurrent={1}
        onChange={handlePageChange}
        total={40}
        className="!my-[50px] text-center"
      />
    </div>
  )
}

export default AdminTemplate
