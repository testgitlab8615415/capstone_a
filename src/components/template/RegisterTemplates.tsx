import { useEffect } from "react";
import { DatePicker } from "antd";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { authServices } from "services";
import { RegisterSchema, RegisterSchemaType } from "schema";
import { SubmitHandler, useForm } from "react-hook-form";
import { PATH } from "constant";
import { zodResolver } from "@hookform/resolvers/zod";
import { Register } from "components/ui";


const RegisterTemplates = () => {
  useEffect(() => {
    const inputElement = document.getElementById(
      "ngay-sinh"
    ) as HTMLInputElement;
    if (inputElement) {
      inputElement.addEventListener("keydown", (e) => {
        e.preventDefault();
      });
    }
  }, []);

  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema),
  });

  const navigate = useNavigate();

  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    try {
      console.log(value);

      await authServices.Register(value);
      toast.success("Đăng ký thành công!");
      navigate(PATH.login);
    } catch (err) {
      toast.error("Đăng kí không thành công");
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-white text-40 font-600">Đăng ký</h2>
      <div className="mt-30">
        <Register
          register={register}
          name="name"
          error={errors?.name?.message}
          placeholder="Tên"
          type="text"
        />
        <div className="mt-20">
          <Register
            register={register}
            name="email"
            error={errors?.email?.message}
            placeholder="Email"
            type="email"
          />
        </div>
        <div className="mt-20">
          <Register
            register={register}
            name="password"
            error={errors?.password?.message}
            placeholder="Mật khẩu"
            type="password"
          />
        </div>
        <div className="mt-20">
          <Register
            register={register}
            name="phone"
            error={errors?.phone?.message}
            placeholder="Số điện thoại"
            type="text"
          />
        </div>
        <div className="mt-20">
          <DatePicker
            id="ngay-sinh"
            className="w-full p-20"
            placeholder="Ngày sinh"
            format="DD/MM/YYYY"
            onChange={(date, dateString) => {
              setValue("birthday", dateString);
              console.log("Selected Date:", date);
              console.log("Formatted Date String:", dateString);
            }}
          />
        </div>
        <div className="mt-20">
          <h3 className="mb-4 text-18 text-white">Giới tính</h3>
          <ul className="items-center w-full text-sm  text-gray-900 bg-white border border-gray-200 rounded-lg sm:flex ">
            <li className="w-full border-b border-gray-200 sm:border-b-0 sm:border-r ">
              <div className="flex items-center pl-3">
                <input
                  id="horizontal-list-radio-license"
                  type="radio"
                  name="list-radio"
                  className="w-6 h-6"
                  value="Nam"
                />
                <label
                  htmlFor="horizontal-list-radio-license"
                  className="w-full py-3 ml-2 text-sm   "
                >
                  Nam{" "}
                </label>
              </div>
            </li>
            <li className="w-full">
              <div className="flex items-center pl-3">
                <input
                  id="horizontal-list-radio-id"
                  type="radio"
                  name="list-radio"
                  className="w-6 h-6"
                  value="Nữ"
                />
                <label
                  htmlFor="horizontal-list-radio-id"
                  className="w-full py-3 ml-2 text-sm   "
                >
                  Nữ
                </label>
              </div>
            </li>
          </ul>
        </div>

        <div className="mt-16">
          <button className="text-white font-700  bg-red-500 w-full hover:bg-red-800 rounded-lg text-sm px-5 py-[15px]">
            Đăng ký
          </button>
        </div>
      </div>
    </form>
  );
};

export default RegisterTemplates;
