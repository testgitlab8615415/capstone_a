import { useParams } from "react-router-dom"
import { useAppDispatch } from "store"
import { useAuth } from "hooks/useAuth"
import { useEffect } from "react"
import { getPositionByIDThunk } from "store/Position"


const PositionByID = () => {
  const param = useParams()

  const { id } = param
  const dispatch = useAppDispatch()
  const { positionByID } = useAuth()
  useEffect(() => {
    dispatch((getPositionByIDThunk(id)))
  }, [dispatch])
  console.log(positionByID)
  return (
    <div>hello
      {
        <img src={positionByID?.hinhAnh} alt="" />
      }

    </div>
  )
}

export default PositionByID
