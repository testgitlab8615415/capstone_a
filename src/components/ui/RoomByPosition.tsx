import { generatePath, useNavigate, useParams } from "react-router-dom"
import { useAppDispatch } from "store"
import { useAuth } from "hooks/useAuth"
import { useEffect } from "react"
import { getRoomByPositionThunk } from "store/Room"
import { PATH } from "constant"

const RoomByPosition = () => {
    let path = " "
    const navigate = useNavigate()
    const param = useParams()
    const { mavitri } = param
    const dispatch = useAppDispatch()
    const { roomByPosition } = useAuth()
    useEffect(() => {
        dispatch((getRoomByPositionThunk(mavitri)))
    }, [dispatch, mavitri])


    return (
        <div>
            {
                roomByPosition?.map((room) => {
                    return (
                        <div key={room.id}>
                            <h1>{room.moTa}</h1>
                            <img onClick={() => {
                                path = generatePath(PATH.roomByID, {
                                    id: room.id
                                });
                                navigate(path);
                            }} src={room.hinhAnh} alt="" />
                        </div>
                    )
                })
            }
        </div>
    )
}

export default RoomByPosition
