import { HTMLInputTypeAttribute } from "react";
import { UseFormRegister } from "react-hook-form";

type InputProps = {
  register: UseFormRegister<any>;
  error?: string;
  name: string;
  placeholder?: string;
  type?: HTMLInputTypeAttribute;
};

export const Input = ({
  register,
  error,
  name,
  placeholder,
  type,
}: InputProps) => {
  return (
    <div>
      <input
        type={type}
        placeholder={placeholder}
        className="outline-none block w-full p-20 rounded-lg"
        {...register(name)}
      />
      {error && <p className="text-red-500">{error}</p>}
    </div>
  );
};

export default Input;
