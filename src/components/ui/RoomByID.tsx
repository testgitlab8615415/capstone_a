import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch } from "store";
import { useAuth } from "hooks/useAuth";
import { getRoomByIDThunk } from "store/Room";
import { deleteCommentsThunk, getCommentsByRoomThunk } from "store/Comment/thunk";

const RoomByID = () => {
  const { id } = useParams();
  const dispatch = useAppDispatch();
  const { detailList ,commentByRoom,user} = useAuth();
  useEffect(() => {
    dispatch(getRoomByIDThunk(id));
    dispatch(getCommentsByRoomThunk(id))
  }, [dispatch]);
const handleDeleteComment = (id: number) =>{
  dispatch(deleteCommentsThunk(id))
}

  return (

    <div className="container">
      <div className="">
        <div>
          <h2>MÃ PHÒNG : {detailList?.id}</h2>
          <img src={detailList?.hinhAnh} alt="" />
        </div>
        
        <div>
          <h2>Bình Luận </h2>
          {
            commentByRoom?.map((comment)=>{
              return(
                <div key={comment.id}>
                  <h1>{comment.id}</h1>
                  <p>{comment.noiDung}</p>
                  <img src={comment.avatar} alt="" />
                  {user?.user.role=="ADMIN"  && (
                    
                    <button onClick={()=>{
                      handleDeleteComment(comment.id)
                    }}>delete</button>
                  )}
                </div>
              )
            })
          }
          <input type="text" className="w-[200px]" placeholder="Input your comments here" />
        </div>
      </div>



    </div>


  );
}
export default RoomByID;
