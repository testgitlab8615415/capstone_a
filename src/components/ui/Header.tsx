import { NavLink, generatePath, useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { useAppDispatch } from "store";
import { authAction } from "store/auth";


export const Header = () => {
  const { user } = useAuth();
  let path = " "
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  return (
    <div style={{ maxWidth: 1000, margin: "auto", display: "flex", alignItems: "center", justifyContent: "space-between" }}>
      <div className="icon" >
        <img style={{ width: 50 }} src="   public/img/logo.png" alt="" /></div>
      <div >
        <div className="flex items-center justify-around">
          {user?.user.role == "ADMIN" && (
            <div className="flex items-center justify-around">
              <NavLink className="mr-40" to={PATH.admin}>
                Admin
              </NavLink>
            </div>
          )}
          <NavLink className="mr-40" to={"/"}>
            About
          </NavLink>
          <NavLink to={"PATH.contact"}>Contact</NavLink>


          {user && (

            <Popover
              content={
                <div className="p-10">
                  <h2
                    className="font-600 mb-10 p-10 cursor-pointer "
                    onClick={() => {
                      navigate("/");
                    }}
                  >
                    {user?.user.name}
                  </h2>
                  <hr />
                  <div
                    className="!p-10 !mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                    onClick={() => {
                      path = generatePath(PATH.account, {
                        id: user.user.id,
                      });
                      navigate(path);
                    }}
                  >
                    Thông tin tài khoản
                  </div>
                  <div
                    className="p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white rounded-lg transition-all duration-300"
                    onClick={() => {
                      dispatch(authAction.logOut());
                    }}
                  >
                    Đăng xuất
                  </div>
                </div>
              }
              trigger="click"
            >
              <Avatar
                className="!ml-40 !cursor-pointer !flex !items-center !justify-center"
                size={30}
                icon={<UserOutlined />}
              />
            </Popover>
          )}
          {!user && (
            <p
              className="font-600 text-16 ml-40 cursor-pointer"
              onClick={() => {
                navigate(PATH.login);
              }}
            >
              Login
            </p>
          )}
        </div>
      </div>
    </div>
  )
}

export default Header
